// importing dependencies
const express = require('express');
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const cors = require('cors');
const socketIO = require('socket.io');
const http = require('http');

// expressjs
const app = express();

app.use(
    bodyParser.urlencoded({
        extended: false
    })
);
app.use(bodyParser.json());
app.use(cors());

//const users = require('./routes/api/users');
//app.use("/api/users", users);

app.get('/', (req, res) => {
    res.send('ClouDL server up and running.');
});

// socketio
const server = http.createServer(app);
const io = socketIO(server, {
    path: '/socket/'
});

let sockets = [];
let python_socket;

io.on('connection', socket => {
    console.log('New client connected!');
	
    if(socket.handshake.headers.name === 'python'){
        python_socket = socket;
    }
	
    socket.on('userId', user => {
    	sockets[user.userId] = socket;
    })

    if (python_socket) {
        socket.on('newJob', job => {
            let {id, link} = job;

            python_socket.emit('new_job', {id, link})
        });

        python_socket.on('job_add_success', userId => {
	          let userSocket = sockets[userId];
            userSocket.emit('job_add_success');
        });
    }
});

// mongodb
mongoose
    .connect(
        `mongodb://mongodb-service/cloudl`, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        })
    .then(() => console.log("MongoDB successfully connected"))
    .catch(err => console.log(err));

// fireup the server
const port = process.env.PORT || 5000;

server.listen(port, () => console.log(`Server up and running on port ${port} !`));